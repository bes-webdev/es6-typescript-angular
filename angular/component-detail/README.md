# Angular (partie 6) : Quelques détails sur les `Component`.

Nous avons déjà beaucoup parlé des `Component` mais il reste encore deux trois détails importants dont nous n'avons pas encore parlé.

## Le _decorator_ `@Input()`

Ce _decorator_ est à placer sur une propriété du `Component`. Il nous permet de déclarer des variables à préciser lorsque nous faisons appel à notre `Component` via son `selector`.

Pour l'exemple j'ai créé un `Component` [_blog-post-comments_](mon-premier-projet/src/app/blog-post-comment/blog-post-comment.component.ts). Il sert à afficher les commentaires d'un post de blog. Et pour pouvoir afficher ces commentaires, il doit savoir de quel post de blog on parle. Donc on lui a créé une propriété `blogPost: BlogPost` décorée avec `@Input()` que son component parent va devoir lui préciser quand il fait appel à son `selector` (en mettant un attribut entouré de []). Vous pouvez voir comment on fait [ici](mon-premier-projet/src/app/blog-post/blog-post.component.html) 

## Le _decorator_ `@Output` et `EventEmmiter`

Ce _decorator_ au contraire du précédent va nous permettre de préciser ce que le component va envoyer comme données à son parent. Il va le faire via un évènement et son parent va devoir l'écouter toujours via son selector en précisant la _method_ de _callback_ associée. Il le fera avec ce genre de syntaxe `([eventName])="methode($event)"`.

Dans cet exemple  j'écoute un énènement concernant le nombre de likes dans [blog-post](mon-premier-projet/src/app/blog-post/blog-post.component.ts) depuis [blog-post-comment](mon-premier-projet/src/app/blog-post/blog-post.component.ts)
