import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BlogPost} from '../shared/blog-post.model';

@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.css']
})
export class BlogPostComponent implements OnInit {


  blogPost: BlogPost;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(params => {
        // Imaginions que nous récupérons le post sur le server en se basant sur son id.
        // A la place pour le moment nous allons en hardcoder un
        this.blogPost = new BlogPost(params['id'], "Titre de mon premier post", "bla bla bla bla",2)
      });
  }

  adaptLikes($event: number) {
    this.blogPost.likes = $event;
  }
}
