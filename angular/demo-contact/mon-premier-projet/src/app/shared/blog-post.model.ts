export class BlogPost {
  id?: number;
  title?: string;
  body?: string;
  likes?: number;


  constructor(id: number, title: string, body: string, likes: number) {
    this.id = id;
    this.title = title;
    this.body = body;
    this.likes = likes;
  }
}
