import {Component, Input, OnInit} from '@angular/core';
import {BlogPost} from "../shared/blog-post.model";
import {BlogPostServiceService} from "../blog-post/blog-post-service.service";
import {HttpResponse} from "@angular/common/http";

@Component({
  selector: 'app-blog-post-summary',
  templateUrl: './blog-post-summary.component.html',
  styleUrls: ['./blog-post-summary.component.css']
})
export class BlogPostSummaryComponent {

    @Input()
    blogPost: BlogPost;

}
