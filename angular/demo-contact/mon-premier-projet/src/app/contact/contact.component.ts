import { Component, OnInit } from '@angular/core';
import {ContactModel} from '../shared/contact.model';
import {BlogPostServiceService} from '../blog-post/blog-post-service.service';
import {ContactFormService} from '../home/ContactFormService';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactMessage: ContactModel = new ContactModel();

  contactSuccess: Boolean = false;
  contactSubmit: Boolean = false;

  constructor(private contactFormService: ContactFormService) { }

  ngOnInit() {
  }



  private isContactValid() {
    return this.contactMessage.email && this.contactMessage.message;
  }

  isEmailValid() {
    return true;
    // return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.contactMessage.email);
  }
  isMessageValid() {
    return this.contactMessage.message.length > 10;
  }

  postContactForm() {
    if (this.isContactValid()) {
      this.contactSubmit = true;
      this.contactFormService.postContactForm(this.contactMessage)
        .subscribe(
          (res) => {
            this.contactSuccess = true;
            this.contactSubmit = false;
          },
          () => this.contactSuccess = false
        );
    }
  }
}
