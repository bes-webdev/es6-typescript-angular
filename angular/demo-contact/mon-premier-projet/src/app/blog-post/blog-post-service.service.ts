import {Injectable} from '@angular/core';

import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {BlogPost} from "../shared/blog-post.model";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BlogPostServiceService {

  SERVER_URL = "https://prof2.bes-webdeveloper-seraing.be/blog/back/api/";

  private httpHeaders = new HttpHeaders({
      'Content-Type':  'application/json',
      'Accept': 'application/json',

    });

  constructor(private http: HttpClient ) { }



  findAll(): Observable<HttpResponse<BlogPost[]>> {
    return this.http.get<BlogPost[]>(this.SERVER_URL+"blog_posts", {headers: this.httpHeaders, observe: 'response'}) ;
  }

  find(id: number): Observable<HttpResponse<BlogPost>> {
    return this.http.get<BlogPost>(this.SERVER_URL + "blog_posts/" + id, {headers: this.httpHeaders, observe: 'response'});
  }
}
