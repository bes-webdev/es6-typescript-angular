import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ContactModel} from '../shared/contact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactFormService {
  SERVER_URL = "http://localhost:8080/cms/api.php?entity=contactMessage";

  private httpHeaders = new HttpHeaders({
    'Content-Type':  'application/json',
    'Accept': 'application/json',

  });

  constructor(private http: HttpClient ) { }

  postContactForm(contactMessage: ContactModel) : Observable<HttpResponse<ContactModel>> {
    return this.http.post<ContactModel>(this.SERVER_URL, contactMessage, {observe: 'response'});
  }
}
