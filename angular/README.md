# Angular
Nous allons maintenant parler d'Angular. Comme support nous utiliserons ce [cours](https://openclassrooms.com/fr/courses/4668271-developpez-des-applications-web-avec-angular) d'openclassrooms.com.

## Framework côté client
[Angular](https://angular.io/) est un [framework](https://fr.wikipedia.org/wiki/Framework) javascript développé par Google. 

Le principe du framework est d'imposer un cadre de travail (d'où le nom framework) pour développer notre logiciel. Quand nous utilisons Angular nous devons utiliser la "manière Angular" de faire notre application.

Cela veut dire que nous devrons placer nos fichiers aux bons endroits et respecter les règles prévues par le framework en utilisant les composants mis à notre disposition. 
Le but est donc (en plus de connaître de language) de bien comprendre comment s'imbriquent les composants et de bien comprendre comment nous devons les utiliser.

A côté de ces contraintes un framework nous rend aussi des services: 

- Il nous soulage de nombreuses questions de conception. 
- Il nous facilite le travail en équipe.
- Il nous permet par conséquent de gagner du temps de développement.


Je vous invite dès à présent à rentrer dans le vif du sujet avec la [première installation et la découverte de la structure des fichiers](introduction).
