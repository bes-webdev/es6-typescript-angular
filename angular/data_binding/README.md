# Angular (partie 3) : Data binding

Le data binding est à considérer comme une liaison des données. Cette liaison se fait entre le fichier `.ts` de votre _component_ et son fichier `.html`.

## Afficher les variables dans le template

Le template de votre _component_ est son fichier html. Pour afficher une variable déclarée dans votre fichier `.ts` vous devez simplement l'écrire entre double accolades `{{maVariable}}`. Cette liaison permet à l'affichage d'être à jour avec la valeur de la variable affichée. Si la valeur de la variable change dans le fichier `.ts` son affichage sera mis à jour instantanément.

Je vous propose de voir ça en live avec la variable compteur qui que j'ai ajouté au fichier `app.component.ts` (je vous laisse le retrouver tout seuls et lancer l'angular tout seuls aussi).


## Et les évènements dans tout ça

Hé bien pour les évènements on va les déclarer depuis le template, et comme on a toujours fait, les associer à une _function_ (une _method_ de notre _component_ pour etre exact). 
 
Cliquez sur le bouton _reset_ à coté de l'affichage du compteur et vous verrez que la _function_ `reset()` a été appelée.

Dans le fichier de template vous constatez qu'on a simplement écrit le nom de l'évènement et sa methode associée comme un attribut html un peu bizarre `(click)="reset()"`. 
