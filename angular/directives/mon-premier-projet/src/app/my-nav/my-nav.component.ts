import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent implements OnInit {

  isAuth: boolean;

  constructor() { }

  ngOnInit() {
    this.isAuth = false;
  }

  logIn(){
    this.isAuth = true;
  }

  logOff(){
    this.isAuth = false;
  }

}
