import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'mon-premier-projet';

  compteur: number;

  fruits = ['pomme', 'poire', 'peche', 'prune', "fraise"];

  ngOnInit() {
    this.compteur = 0;
    setInterval(
        () => {
          this.compteur ++; // A chaque seconde on incrémente le nombre compteur de 1
        }, 1000
    );
  }

  reset(){
      this.compteur = 0;
  }


}
