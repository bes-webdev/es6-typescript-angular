# Angular (partie 4) : Les directives

Les directives, comme très bien écrit [ici](https://openclassrooms.com/fr/courses/4668271-developpez-des-applications-web-avec-angular/5088481-structurez-le-document-avec-des-directives) sont des **instructions intégrées au dom**. Un peu comme les event du chapitre précédent.

Il en existe deux types.

## Les directives structurelles

Ce sont des directives qui modifient la structure du document. Il y a les conditions et les boucles.

### Les conditions : *ngIf

Avec `*ngIf` vous pouvez utiliser des conditions dans vorte template. Vous mettez simplement la condition dans la valeur de l'attribut.

Allez jeter un oeil dans le component `my-nav` voir comment je m'y suis pris.

### les boucles : *ngFor

Avec `*ngFor` vous créez cette fois des boucles dans vos templates. Nous utilison la syntaxe `let ... of ...` dans la valeur de l'attribut `*ngFor`.

Allez jeter un oeil cette fois dans le component `app`.

## Les directives par attribut

Ce sont des directives qui permettent d'utiliser des expressions comme valeur d'attribut. Elles modifient un élement existant. Comme dans la source d'openclassrooms nous allons voir ngClass et ngStyle

### ngClass

`[ngClass]=""` permet d'attibuer des class à notre élement dom de manière dynamique. En valeur on lui donne un objet dont les clés sont le nom de la classe et la valeur un boolean lui indiquant si la classe doit etre présente ou non. regardez le premier `li` de ma liste de fruits dans le component `app`.

Vous pouvez bien sur remplacer la valeur `true` ou `false` par une condition.

### ngStyle
`[ngStyle]=""` fonctionne sensiblement de la meme manière. On donne un argument un objet contenant les attributs css sous forme de clé valeur. 

Regardez ce que j'ai fait dans `my-nav` sur l'élement `nav`.
