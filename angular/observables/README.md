# Angular (partie 8) : Les Observables.

## Le _design pattern_
Observable est un _design pattern_ (patron de conception). C'est une manière de faire les choses. Comme son nom l'indique le principe est d'observer des changements d'états d'un sujet _observable_. Il y a donc un _Observable_ et un _Observer_.

Plus tot dans l'année nous avons parlé de _callback_, ces fonctions de rappel qui s'executent à un moment donné. Nous les avions vus avec les requetes asynchones, avec les eventListeners, etc.. Ici c'est une autre façon sandardisée de faire ce genre de chose. On s'abonne aux changements d'états et on execute une méthode en cas de besoin.

## RxJS
RxJs est une librairie qui nous offre une api toute faite pour utiliser le pattern en JS. La libraire offre un object `Observable` et tout un tas de fonctions.

### Observable

Imaginez l'`Observable` comme un object qui envelopper un autre dont la valeur va évoluer. `Observable<string>` est un observable qui va nous renvoyer des objects de type ...`string`. `Observable<string[]>` est un observable qui va nous renvoyer des valeurs de type array de string, etc.. Je pense que vous avez compris le concept.

### .subscribe()

subscribe est une methode de l'object `Observable` qui peut prendre jusqu'à trois arguments.

Le permier est la methode qui va s'executer à chaque changement d'état. Elle prend en argument ce qu'_englobe_ l'observable. Voyez cette exemple :
```
maMethod: Observable<string[]> {
    // Du code qui retourne un observable
}

maMethod.subscribe(
    (value: string[]) => {
    // Ici value est un array de string 
    }
);
```

Le deuxième argument est la methode qui va s'executer en cas d'erreur. L'erreur est donnée en argument de cette method.
```
maMethod: Observable<string[]> {
    // Du code qui retourne un observable
}

maMethod.subscribe(
    (value: string[]) => {
    // Ici value est un array de string 
    },
    (error) => {
        // Ici on peut faire quelque chose de l'erreur
    }
);
```

Le troisième argument est la methode qui va s'executer quand l'observable est fini. Elle ne prend aucun argument.
```
maMethod: Observable<string[]> {
    // Du code qui retourne un observable
}

maMethod.subscribe(
    (value: string[]) => {
    // Ici value est un array de string 
    },
    (error) => {
        // Ici on peut faire quelque chose de l'erreur
    },
    () => {
        // Ici on sait que l'obersvable est terminé.
    }
);
```

C'est donc comme ça que ça se passe côté _observer_. Côté _observable_ par contre on dispose de trois méthodes.
`.next()`, `.error()`, et ... `.complete()`.

Petit exemple avec l'observable (qui ne représente pas un exemple de la vie réelle) :
```
eleves(): Observable<string> {
    const eleves = ['Paul', 'John', 'Ringo', 'Georges'];
    return new Observable<string>(observer => {
        eleves.forEach(eleve => observer.next(eleve));
        observer.complete();
    });
}

eleves().subscribe(
    (value: string) => {
        // Cette methode va être appellée autant de fois qu'il y a d'élèves, avec en argument chaque eleve un après l'autre. 
    },
    (error) => {
        // Ici on peut faire quelque chose de l'erreur
    },
    () => {
        // Ici on sait que l'obersvable est terminé.
    }
);
```

Vous l'avez peut-être compris, c'est l'appel à la methode `subscribe()` qui "déclenche" l'obersvable.

## Et Angular dans tout ça ?

Concrètement si nous voyons tout ça c'est parce que le composant `HttpClient` de Angular (qui fait des requêtes asynchones vers un server) nous retourne des `Observable`.

En gros il fait sa requête, si il a une réponse et que tout se passe bien il appelle `.next()` avec sa réponse. Si la réponse est une erreur il appelle `.error()`. Ensuite il appelle `.complete()`.

Nous avons vu un exmple de comment l'utiliser au travers du service [blogPostService](mon-premier-projet/src/app/blog-post/blog-post-service.service.ts) et son utilisation dans [blogPostComponent](mon-premier-projet/src/app/blog-post/blog-post.component.ts).


## Plus loin avec les pipe()

Pour être un minimum opérationnels nous devons parler des `pipe()`. Les pipes servent quand on a besoin de la réponse d'un observable pour faire autre chose. Nous en avons utilisé  dans [blogPostComponent](mon-premier-projet/src/app/blog-post/blog-post.component.ts). Nous avions besoin de la valeur du paramètre `id` de la route pour savoir quel argument donner à notre service faire appel à notre api et récupérer le bon blog post.

Nous avons donc fait un pipe() sur notre observable et utilisé dedans l'_opérateur_ `mergeMap` qui prend un observable en argument et en retourne un autre.

Dans ce cas, tant qu'on n'a pas fait de subscribe, __aucun__ observable ne se déclenche. Si un de ces observable déclanche une erreur, la chaîne s'arrête et on se retrouve directement dans le deuxième argument du `.subscribe()`


## En conclusion

Il y a encore énormément à dire sur RxJS. Pour démarrer nous allons nous contenter de ce minimum mais il y a tout un tas de ressources disponible pour en apprendre plus.

https://rxjs-dev.firebaseapp.com/guide/overview

https://openclassrooms.com/fr/courses/4668271-developpez-des-applications-web-avec-angular/5089331-observez-les-donnees-avec-rxjs


