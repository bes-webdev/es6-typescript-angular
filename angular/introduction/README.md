# Angular (partie 1)

## Installation
Pour installer Angular nous allons utiliser [npm](../../npm) et installer [angular-cli](https://cli.angular.io/). Angular-cli est est logiciel à utiliser dans le terminal qui va nous permettre de créer et de faire évoluer notre projet Angular.


Ouvrez votre terminal et tapez la commande pour installer **globalement** (-g) angular-cli : `npm install -g @angular/cli` 

Maintenant que c'est fait, nous allons nous déplacer dans le dossier dans lequel nous voulons créer notre projet angular et taper : `ng new mon-premier-projet`.
Tapez `y` pour installer le router Angular et "enter" ensuite pour utiliser du CSS sans pré-processeur (cela fera partie d'un cours que nous ferons plus tard).

Félicitations vous avez créé votre premier projet Angular !

Tapez ces deux commandes pour apprécier les résultats de vos efforts :

    cd mon-premier-projet
    ng serve --open

Votre navigateur devrait ouvrir un onglet pointant vers http://localhost:4200/ et vous affichant la page par défaut d'Angular. 
## Structure des fichiers
Si vous vous déplacez dans [mon-premier-projet](mon-premier-projet) vous verrez qu'il y a tout un tas de fichiers et deux dossiers. Examinons un peu cela...

Pour le moment nous allons nous contenter de scruter seulement certains d'entre eux :

#### package.json
Le plus évident. Nous l'avons vu dans la partie sur [npm](../../npm). C'est le fichier qui répertorie toutes les bibliothèques nécéssaires à notre projet. Si vous l'ouvrez vous constaterez que le framework est en fait un ensemble de librairies.

#### tsconfig.json, tsconfig.spec.json et tslint.json
Ce sont des fichiers qui vont servir à configurer typescript et la manière dont nous pourrons le "syntaxter" (tslint.json)

#### e2e
E2E veut dire "End to End" c'est une manière de tester notre application. Nous verrons cela dans un autre cours.

#### src
Nous y sommes enfin ! C'est l'endroit ou nous allons mettre notre code source

#### src/index.html
C'est le point d'entrée de notre application.

#### src/assets
C'est un dossier dans lequel nous pourrons mettre des fichiers statiques. Par exemple nos images.

#### src/app
C'est le **module** par défaut créé par angular. Dans ce dossiers nous voyons un fichier `module`, un fichier `component`, un fichier de `routing`, un fichier html et un de css. 

Nous remarquons aussi qu'il y a une convention sur la manière de nommer nos fichiers nom.type.extension .
 
Un regard sur ce dossier nous donne un premier aperçu de la [manière dont le framework fonctionne](../structure_du_framwork).
