# Les Forms

Pour ce chapitre nous allons nous inspirer de ce qui est expliqué [ici](https://openclassrooms.com/fr/courses/7471271-completez-vos-connaissances-sur-angular/7549521-ecoutez-attentivement-vos-utilisateurs-avec-les-reactive-forms) dans le cours d'openclassroms.

## La méthode template

La méthode template est la plus simple, c'est aussi celle sur laquelle nous avons le moins de contrôle. Je vais donc vous laisser simplement lire [le chapitre d'openclassrooms](https://openclassrooms.com/fr/courses/7471271-completez-vos-connaissances-sur-angular/7549511-creez-un-formulaire-basique-avec-les-template-forms) et me concentrer sur la methode la plus complète.

## La méthode reactive

La méthode _reactive_ est une méthode dans laquelle nous allons créer des objects pour chaque entrée de notre formulaire, nous allons aussi leur déclarer (ou pas) une validation. Angular validera le formulaire à notre place et ajoutera des classes css pour les entrée valides et invalides. Nous écouterons simplement la soumission du formulaire, assignerons ces valeurs à notre modèle avant de pouvoir le fournir à un service quelconque pour l'envoyer vers un server.

### Le FormBuilder

Le `FormBuilder` est un service fourni par le module `Form` d'Angular. Comme son nom l'indique, il s'agit d'un service qui va nous aider à fabriquer nos formulaires. Il faut donc le déclarer en dépendance de notre component. Pour le faire il faut vérifier que le `FormsModule` et `ReactiveFormsModule` soient bien importés dans notre module.

Ici pour le [app.module](./mon-premier-projet/src/app/app.module.ts)
```
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// ...

@NgModule({
  declarations: [
    AppComponent,
    MyNavComponent,
    HomeComponent,
    BlogPostComponent,
    BlogPostCommentComponent,
    BlogPostSummaryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
```

Ensuite l'injection dans notre [blog-post-comment.component.ts](./mon-premier-projet/src/app/blog-post-comment/blog-post-comment.component.ts)

```
import { FormBuilder, FormGroup } from '@angular/forms';
// ...

constructor(private formBuilder: FormBuilder) { }
```

### Déclarer le form

Ici on peut voir qu'on a importé une classe dont on n'a pas encore parlé, le `FormGroup`. Vous vous êtes certainement demandé ce que c'était.

En gros le `FormBuilder` va nous aider à construire un `<form>` en fait être représenté par un `FormGroup` (considérez le comme un groupe d'entrées) qui lui même sera composé d'un ou plusieurs `FormControl`. Ces `FormControl` représenteront les entrées de notre formulaire final.

Dans notre [blog-post-comment.component.ts](./mon-premier-projet/src/app/blog-post-comment/blog-post-comment.component.ts) nous allons initialiser (déclarer) le form

```
commentForm: FormGroup;

// ...
ngOnInit() {
    this.initForm();
}

// ...
initForm() {
    this.commentForm = this.formBuilder.group({
      author: ['', Validators.required],
      message: ['', Validators.minLength(4)]
    });
  }
```

Expliquons un peu ces quelques lignes de code :
J'assigne à mon `FormGroup` le résultat de la method `group()` du `FormBuilder`. Elle sert donc à créer un groupe.

Cette _method_ prend en argument un _object_ dont les clés sont les noms des entrées (des `FormControl`) et en valeur un _tableau_ avec en premier la valeur par defaut de l'entrée et en deuxième le type de validation qui lui sera assignée. Il y a plein de validations possibles que vous pouvez retrouver [dans la documentation](https://angular.io/api/forms/Validators). Vous pouvez même créer [vos propres validators](https://angular.io/guide/form-validation#custom-validators) pour les plus avancés. 

J'ai donc créé ici un formulaire avec deux entrées: `author` et `message`.

### Afficher le form

Pour afficher le _form_ il nous faut évidemment aller dans le template et ajouter quelques lignes:

```
<div>
    <h2>Ajouter un commentaire</h2>
    <form [formGroup]="commentForm" (ngSubmit)="onSubmitForm()">
      <div class="form-group">
        <label for="author">Nom</label>
        <input type="text" id="author" class="form-control" formControlName="author">
      </div>
      <div class="form-group">
        <label for="message">Nom</label>
        <textarea id="message" class="form-control" formControlName="message"></textarea>
      </div>
      <button type="submit" class="btn btn-primary">Soumettre</button>
    </form>
  </div>
```

Pour commencer dans la balise `form` nous utilisons la directive `[formGroup]` pour _data-binder_ notre formulaire à notre propriété `commentForm`. Nous allons aussi écouter l'_event_ `(ngSubmit)` qui se déclenchera quand l'utilisateur soumettera son formulaire. 
Pour chaque entrée correspondant à un `FormControl` nous utiliserons la directive `formControlName` pour dire au module quel _input_ sera lié à quel `FormControl`.

### Ecouter la soumission du formulaire

Retournons dans le controller pour écouter la soumission du formulaire :

```
onSubmitForm() {
    const formValue = this.commentForm.value;
    const newComment = new Comment(
      null,
      formValue['author'],
      formValue['message'],
      new Date()
    );
    // Imaginons que nous envoyons notre nouveau commentaire vers un server
    // this.commentService.create(newComment)
    //  .subscribe(res => {
    //    //rafraichissement de la liste des commentaires
    // })
    //;
  }
```    
D'abord nous récupérons les valeurs des `FormControl` depuis la propriété `value` de notre `FormGroup`. Ces valeurs sont stockées dans un _object_ qui a une clé par _control_ qui va stocker la valeur du _control_.

Nous instencions simplement un nouveau commentaire en lui donnant les valeurs du formulaire. Ensuite éventuellement nous envoyons notre nouveau commentaire vers le server.

### Comment marche la validation ?

Pour être certains que notre formulaire ne sois soumis que lorsqu'il est valide, nous pouvons modifier notre bouton de soumission comme ceci :
`<button type="submit" class="btn btn-primary" [disabled]="userForm.invalid">Soumettre</button>`

La propriété `invalid` d'un `FormGroup` est un _bool_ qui est true tant que le formulaire n'est pas valide.

#### Utiliser les classes mises à jour par Angular.

Sur chaque input lié à un `FormControl` le `ReactiveFormsModule` va ajouter des classes en fonction de l'état du _control_.

- dirty: Cet état indique que la valeur de l'input change. Pour marquer un input _dirty_ la classe `ng-dirty` est directement ajoutée
- touched: Cet état indique si l'input a subi un évenement de type _blur_, c'est à dire que l'élément a eu, puis perdu le focus. Les classes sont `ng-touched` ou `ng-untouched`
- valid: Pour indiquer si la valeur est valide avec `ng-valid` et `ng-invalid`.

Nous pouvons donc jouer avec la css pour faire un truc du genre:

`input.ng-dirty.ng-invalid.ng-touched, textarea.ng-dirty.ng-invalid.ng-touched { border: 1px solid red;}`

#### Ajouter des messages d'erreur

Nous pouvons aussi faciliter la vie de l'utilisateur en ajoutant des messages d'erreurs spécifiques aux validations :

```
<div class="form-group">
    <label for="author">Nom</label>
    <input type="text" id="author" class="form-control" formControlName="author">
    <div *ngIf="contactForm.controls.author.invalid && (contactForm.controls.author.dirty || contactForm.controls.author.touched)"
         <div *ngIf="contactForm.controls.author.errors?.required" class="alert alert-danger">
            Votre nom est obligatoire
          </div>
    </div>
</div>
```
Ici nous avons simplement ajouté un `div` qui ne s'affiche que si le _control_ est invalide et qu'il a été _touched_ ou est en cours de modification (_dirty_).
Dedans nous avons ajouté un autre dic avec la class `alert` pour le cas spécifique d'une erreur sur le validator _required_ (le seul que nous avons mis sur ce control).µ

Nous récupérons le control via la propriété `controls` d'un formGroup comme ceci `leFormGroup.controls.leControl`. Sa propriété errors contient quelque chose uniquement si il y a des erreurs de validation.

#### Plusieurs validators par FormControl

Pour définir plusieurs validators par `FormControl` il suffit simplement de fournir un tableau de `Validator` à la déclaration de notre `FormControl`. En effet le builder accepte soit un Validator soit un tableau de Validators comme deuxième argument.

```
commentForm: FormGroup;

// ...
ngOnInit() {
    this.initForm();
}

// ...
initForm() {
    this.commentForm = this.formBuilder.group({
      author: ['', [Validators.required, Validators.minLength(3)]],
      message: ['', Validators.minLength(4)]
    });
  }
```
