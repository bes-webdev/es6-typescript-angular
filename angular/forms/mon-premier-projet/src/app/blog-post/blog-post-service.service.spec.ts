import { TestBed } from '@angular/core/testing';

import { BlogPostServiceService } from './blog-post-service.service';

describe('BlogPostServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BlogPostServiceService = TestBed.get(BlogPostServiceService);
    expect(service).toBeTruthy();
  });
});
