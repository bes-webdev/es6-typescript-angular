"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var BlogPostServiceService = /** @class */ (function () {
    function BlogPostServiceService(http) {
        this.http = http;
        this.SERVER_URL = "https://prof2.bes-webdeveloper-seraing.be/blog/back/api/";
        this.httpHeaders = new http_1.HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        });
    }
    BlogPostServiceService.prototype.findAll = function () {
        return this.http.get(this.SERVER_URL + "blog_posts", { headers: this.httpHeaders, observe: 'response' });
    };
    BlogPostServiceService.prototype.find = function (id) {
        return this.http.get(this.SERVER_URL + "blog_posts/" + id, { headers: this.httpHeaders, observe: 'response' });
    };
    BlogPostServiceService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], BlogPostServiceService);
    return BlogPostServiceService;
}());
exports.BlogPostServiceService = BlogPostServiceService;
