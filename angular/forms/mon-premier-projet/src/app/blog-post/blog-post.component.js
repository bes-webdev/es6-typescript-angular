"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var operators_1 = require("rxjs/operators");
var BlogPostComponent = /** @class */ (function () {
    function BlogPostComponent(activatedRoute, blogPostService) {
        this.activatedRoute = activatedRoute;
        this.blogPostService = blogPostService;
    }
    BlogPostComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params
            .pipe(operators_1.mergeMap(function (params) {
            return _this.blogPostService.find(params['id']);
        }))
            .subscribe(function (res) { return _this.blogPost = res.body; });
    };
    BlogPostComponent.prototype.adaptLikes = function ($event) {
        this.blogPost.likes = $event;
    };
    BlogPostComponent = __decorate([
        core_1.Component({
            selector: 'app-blog-post',
            templateUrl: './blog-post.component.html',
            styleUrls: ['./blog-post.component.css']
        })
    ], BlogPostComponent);
    return BlogPostComponent;
}());
exports.BlogPostComponent = BlogPostComponent;
