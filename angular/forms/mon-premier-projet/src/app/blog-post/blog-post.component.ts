import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BlogPost} from '../shared/blog-post.model';
import {BlogPostServiceService} from "./blog-post-service.service";
import {map, mergeMap} from "rxjs/operators";
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.css']
})
export class BlogPostComponent implements OnInit {


  blogPost: BlogPost;

  constructor(private activatedRoute: ActivatedRoute, protected blogPostService: BlogPostServiceService) { }

  ngOnInit() {
    this.activatedRoute.params
      .pipe(
        mergeMap(params => {
          return this.blogPostService.find(params['id']);
        })
      )
      .subscribe(res =>  this.blogPost = res.body);
  }

  adaptLikes($event: number) {
    this.blogPost.likes = $event;
  }
}
