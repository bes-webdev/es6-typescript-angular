"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var my_nav_component_1 = require("./my-nav/my-nav.component");
var home_component_1 = require("./home/home.component");
var blog_post_component_1 = require("./blog-post/blog-post.component");
var blog_post_comment_component_1 = require("./blog-post-comment/blog-post-comment.component");
var common_1 = require("@angular/common");
var blog_post_summary_component_1 = require("./blog-post-summary/blog-post-summary.component");
var http_1 = require("@angular/common/http");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                my_nav_component_1.MyNavComponent,
                home_component_1.HomeComponent,
                blog_post_component_1.BlogPostComponent,
                blog_post_comment_component_1.BlogPostCommentComponent,
                blog_post_summary_component_1.BlogPostSummaryComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                common_1.CommonModule,
                FormModule,
                http_1.HttpClientModule
            ],
            providers: [],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
