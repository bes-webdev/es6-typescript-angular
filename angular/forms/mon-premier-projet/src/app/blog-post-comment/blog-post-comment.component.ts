import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Comment} from '../shared/comment.model';
import {BlogPost} from '../shared/blog-post.model';

@Component({
  selector: 'app-blog-post-comment',
  templateUrl: './blog-post-comment.component.html',
  styleUrls: ['./blog-post-comment.component.css']
})
export class BlogPostCommentComponent implements OnInit {
  comments: Comment[];

  commentForm: FormGroup;

  @Input()
  blogPost: BlogPost;

  @Output()
  // En précisant number entre <> on précise que notre event va envoyer une donnée de type number
  likesChanged: EventEmitter<number> = new EventEmitter<number>();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    //Imaginons que nous devons récupérer les comments du post depuis cet endroit (ici nous allons simplement créer une liste de comments statique
    this.comments = [
      new Comment(1,'John Doe', 'this is my comment :+1:', new Date(2020,1,12,9, 52)),
      new Comment(1,'Jane Doe', 'I absolutely disagree with John\'s comment' , new Date(2020,1,12,9, 53)),
    ]
  }

  like() {
    // Ici on devrait faire un call api pour ajouter un like
    // Ensuite on dit que le nombre a changé, pour l'exemple on donne le nombre de nouveaux likes en paramètre
    this.likesChanged.emit(this.blogPost.likes ++);
  }

  initForm() {
    this.commentForm = this.formBuilder.group({
      author: ['', Validators.required],
      message: ['', Validators.minLength(4)]
    });
  }

  onSubmitForm() {
    const formValue = this.commentForm.value;
    const newComment = new Comment(
      null,
      formValue['author'],
      formValue['message'],
      new Date()
    );
    // Imaginons que nous envoyons notre nouveau commentaire vers un server
    // this.commentService.create(newComment)
    //  .subscribe(res => {
    //    //rafraichissement de la liste des commentaires
    // })
    //;
  }
}
