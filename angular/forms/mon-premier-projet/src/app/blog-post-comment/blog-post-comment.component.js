"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var comment_model_1 = require("../shared/comment.model");
var BlogPostCommentComponent = /** @class */ (function () {
    function BlogPostCommentComponent(formBuilder) {
        this.formBuilder = formBuilder;
        // En précisant number entre <> on précise que notre event va envoyer une donnée de type number
        this.likesChanged = new core_1.EventEmitter();
    }
    BlogPostCommentComponent.prototype.ngOnInit = function () {
        //Imaginons que nous devons récupérer les comments du post depuis cet endroit (ici nous allons simplement créer une liste de comments statique
        this.comments = [
            new comment_model_1.Comment(1, 'John Doe', 'this is my comment :+1:', new Date(2020, 1, 12, 9, 52)),
            new comment_model_1.Comment(1, 'Jane Doe', 'I absolutely disagree with John\'s comment', new Date(2020, 1, 12, 9, 53)),
        ];
    };
    BlogPostCommentComponent.prototype.like = function () {
        // Ici on devrait faire un call api pour ajouter un like
        // Ensuite on dit que le nombre a changé, pour l'exemple on donne le nombre de nouveaux likes en paramètre
        this.likesChanged.emit(this.blogPost.likes++);
    };
    __decorate([
        core_1.Input()
    ], BlogPostCommentComponent.prototype, "blogPost", void 0);
    __decorate([
        core_1.Output()
    ], BlogPostCommentComponent.prototype, "likesChanged", void 0);
    BlogPostCommentComponent = __decorate([
        core_1.Component({
            selector: 'app-blog-post-comment',
            templateUrl: './blog-post-comment.component.html',
            styleUrls: ['./blog-post-comment.component.css']
        })
    ], BlogPostCommentComponent);
    return BlogPostCommentComponent;
}());
exports.BlogPostCommentComponent = BlogPostCommentComponent;
