export class Comment {
  id: number;
  author: string;
  body: string;
  date: Date;


  constructor(id: number, author: string, body: string, date: Date) {
    this.id = id;
    this.author = author;
    this.body = body;
    this.date = date;
  }
}
