"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Comment = /** @class */ (function () {
    function Comment(id, author, body, date) {
        this.id = id;
        this.author = author;
        this.body = body;
        this.date = date;
    }
    return Comment;
}());
exports.Comment = Comment;
