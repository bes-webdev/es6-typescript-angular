"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BlogPost = /** @class */ (function () {
    function BlogPost(id, title, body, likes) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.likes = likes;
    }
    return BlogPost;
}());
exports.BlogPost = BlogPost;
