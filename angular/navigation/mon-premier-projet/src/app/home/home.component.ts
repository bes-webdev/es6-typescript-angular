import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  compteur: number;

  fruits = ['pomme', 'poire', 'peche', 'prune', "fraise"];

  ngOnInit() {
    this.compteur = 0;
    setInterval(
      () => {
        this.compteur ++; // A chaque seconde on incrémente le nombre compteur de 1
      }, 1000
    );
  }

  reset(){
    this.compteur = 0;
  }

}
