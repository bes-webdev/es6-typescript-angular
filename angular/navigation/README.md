# Angular (partie 5) : La navigation

La navigation est ce qui va nous permettre de créer dans notre application ce qu'on appelle parfois un peu grossièrement des "pages".

On va plutot maintenant parler de routes, et nous allons les retrouver telles qu'elles dans les urls qui vont composer la navigation de notre site.

## Le composant `Router`

Lors de la génération de notre application avec _angular-cli_ nous avons fait le choix d'installer le composant `Router` d'angular. Ce composant va nous permettre de créer des routes.

### Route et component
L'idée générale va etre d'associer une url à un `Component`. Une route va toujours définir une url (/contact par exemple) dans la propriété `path` et un `Component` dans la propriété `component`. On ne met pas de slash au début ni à la fin de nos `path`, Angular le gère pour nous.

Il existe d'autres attributs pour les routes que vous pouvez retrouver [ici](https://angular.io/guide/router#configuration).

#### Les paramètres

Nous pouvons créer des paramètes dans nos `path` avec le signe `:`. Ainsi `path: blog-post/:id` définit une url ou id sera une variable. L'url `blog-post/43` sera reconnue par angular comme correspondant à la route et fera appel au `Component` associé.


### Router outlet
Jusqu'à présent nous affichions un `Component` via son attribut `selector` en le plaçant dans une balise html classique à l'intérieur d'un autre template.

Ici pour signifier à Angular ou le `Component` associé à une route doit s'afficher nous devons insérer la balise `<router-outlet></router-outlet>`. Angular va placer le `component` associé à la l'url à l'endroit de cette balise.

### La directive `routerLink`

Grace à cette directive ([rappel](../directives)) que nous placerons sur nos balise `<a>` angular va comprendre vers quelle url doit pointer notre lien.
Nous pouvons fournir à cette directive un string ou un array. Les deux liens ci-dessous pointent vers la meme url.

```
    <a routerLink="/blog-post/43" routerLinkActive="active">Lien 1</a>
    <a routerLink="['blog-post', 43]" routerLinkActive="active">Lien 2</a>
```
Ici `routerLinkActive` désigne une classe à attribuer au lien si le lien est celui correspondant à la route en cours.

### Activated route

Le `Router` d'angular nous fourni un `Service` à injecter dans nos `Component` nous permettant de récupérer les informations concernant la route _activée_ dans notre `Component`.

Nous verrons plus en détail ce que sont les `Service` et l'_injection de dépendence_ mais sans tout comprendre nous pouvons déjà nous en servir.


## En pratique
Dans cette version du projet, j'ai créé deux components, _home_ et _blog-post_ (avec la commande vue [ici](../structure_du_framwork). J'ai aussi créé deux routes [ici](mon-premier-projet/src/app/app-routing.module.ts)) et j'ai placé la balise `<router-outlet></router-outlet>` [ici](mon-premier-projet/src/app/app.component.html).

J'ai aussi utilisé la directive `routerLink` dans le template `my-nav` pour créer les liens. 

Enfin j'utilise le `Service` `ActivatedRoute` dans le component [blog-post](mon-premier-projet/src/app/blog-post/blog-post.component.ts).
