import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BlogPost} from '../shared/blog-post.model';
import {BlogPostServiceService} from "./blog-post-service.service";
import {map, mergeMap} from "rxjs/operators";
import {CommentService} from '../shared/comment.service';
import {Comment} from '../shared/comment.model';

@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.css']
})
export class BlogPostComponent implements OnInit {


  blogPost: BlogPost;
  comments: Comment[];

  constructor(private activatedRoute: ActivatedRoute, protected blogPostService: BlogPostServiceService, protected commentService: CommentService) { }

  ngOnInit() {
    this.activatedRoute.params
      // .subscribe(params => {
      //   this.blogPostService.find(params['id'])
      //     .subscribe(res =>  this.blogPost = res.body );
      // })
      .pipe(
        mergeMap(params => {
          return this.blogPostService.find(params['id']);
        }),
        mergeMap(postRes => {
          this.blogPost = postRes.body;
          return this.commentService.findByPost(this.blogPost);
        })
      )
      .subscribe(
        commentRes =>  this.comments = commentRes.body,
        error => this.handleError(error)
      );
  }

  adaptLikes($event: number) {
    this.blogPost.likes = $event;
  }

  private handleError(error: any) {
    // Do something with the error like an user friendly message
  }
}
