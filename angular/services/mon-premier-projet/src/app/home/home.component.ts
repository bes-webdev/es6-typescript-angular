import { Component, OnInit } from '@angular/core';
import {BlogPost} from "../shared/blog-post.model";
import {BlogPostServiceService} from "../blog-post/blog-post-service.service";
import {HttpResponse} from "@angular/common/http";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  blogPosts?: BlogPost[];

  constructor(private blogPostService: BlogPostServiceService) { }

  ngOnInit() {
    this.blogPostService.findAll()
      .subscribe((res: HttpResponse<BlogPost[]>) => this.blogPosts = res.body );
  }

}
