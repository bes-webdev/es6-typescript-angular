import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {BlogPost} from './blog-post.model';
import {Observable, of} from 'rxjs';
import {Comment} from './comment.model';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  findByPost(post: BlogPost): Observable<HttpResponse<Comment[]>> {
    // Pas d'api donc on ne va pas appeler réellement le server
    const comment = new Comment(1, 'prof', 'Helle, is there anybody in there ?', new Date());
    return of(new HttpResponse({body: [comment]}));
  }
}
