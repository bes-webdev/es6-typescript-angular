# Angular (partie 7) : Les `Service`.

Les services sont simplement des classes qu'on va déclarer comme _service_ dans Angular. Le but et de rendre des parties de code disponible dans le framework à tout `Component` qui en a besoin et d'éviter de **dupliquer du code**.

C'est une architecture assez répandue dans le petit monde des frameworks.

## Providing

Pour rendre un service disponible il faut le fournir depuis le module en l'ajoutant dans le _Decorator_ `@Module` dans le tableau de la proprété `providers`. Vous pouvez toutefois le rendre disponible à tout le framework en précisant la façon dont vous voulez l'_injecter_.

## Injection

Vous pouvez définir la façon dont le service va être _providé_ directement depuis la classe avec le _decorator_ `@Injectable` [voir ici](mon-premier-projet/src/app/blog-post/blog-post-service.service.ts). Ce decorator précise à angular que cette classe est un service et qu'elle sera injectée dans _root_, ce qui va le rendre disponible dans toute l'application dans n'importe quel module.

## Injection de dépendance

Nous ne pouvons pas partler de l'injection sans parler de l'**_injection de dépendance_**.

C'est un _design pattern_ très répandu. Le principe est que chaque composant va déclarer ce dont il a besoin pour fonctionner (dans notre cas les service). Le framework va se charger de comprendre ce dont le composant à besoin, de l'instancier si besoin et de le fournir (l'injecter) à l'instanciation du dit composant.

Donc en gros chaque composant va déclarer ses dépendances (les services dont il a besoin) et Angular va se charger de les lui injecter.

 Dans Angular l'injection de dépendance se fait via le constructeur. On va donner une liste de paramètres au constructeur et Angular va deviner ce qu'il doit fournir pour instancier le composant. Nous avons déjà utilisé plusieurs fois de ce pattern sans le savoir. Par exemple dans [blog-post-component.ts](mon-premier-projet/src/app/blog-post/blog-post.component.ts) ou nous déclarons avoir besoin de `ActivatedRoute`. 
 
## C'est bien mais à quoi ça sert ?

En fait ça sert à ce qu'on veut. Si même code doit être exécuté depuis plusieurs endroits différents du framework, peut-être vaut-il mieux le mettre dans un service. Principalement nous allons créer des services pour récupérer des données depuis le server.

## En pratique.

En pratique j'ai créé une mini API REST avec l'excellent [API Platform](https://api-platform.com/) dont vous pouvez voir la description [ici](https://prof2.bes-webdeveloper-seraing.be/blog/back/api). Une admin est même disponible [ici](https://prof2.bes-webdeveloper-seraing.be/#/blog_posts). 

Le principe est donc que nous allons récupérer les données de cette API via des requêtes asynchones que nous allons executer depuis un [_service_](mon-premier-projet/src/app/blog-post/blog-post-service.service.ts) dédié à ça. Ces données pourront donc être affichées dans nos components. J'ai aussi modifié le [home-component](mon-premier-projet/src/app/home/home.component.ts) pour qu'il affiche un résumé de chaque post (très brièvement :)) et que chaque résumé comporte un lien vers le `Component` qui [affiche un post en détail](mon-premier-projet/src/app/blog-post/blog-post.component.ts).

Chacun de ces `Component` a besoin de pouvoir récupérer des `BlogPost` et a donc besoin du _service_ dédié. Vous pouvez voir que je l'ai **_injecté_** dans ces `Component` vie le constructeur.

Si vous êtes un peu curieux vous verrez des bouts de code un peu bizarres avec des `Observable`, des `HttpResponse`, des `pipe()`,... Ceci fera l'objet du dernier chapitre sur les Observable et RxJS.
