export class Armstrong {
    // readonly, une propriété qui ne peut jamais changer (comme une const)
    // on peut lui donner une valeur à l'instanciation (via le constructeur par exemple) mais ici ce n'est pas nécéssaire
    readonly lastName : string  = "Armstrong";

    firstName: string;
    selected: boolean = false;
    id: number;


    constructor(firstName: string, id: number) {
        this.firstName = firstName;
        this.id = id;
    }
}
