# Plus loin avec Typescript

Typescript est une surcouche à Javascript. Cette surcouche a été développée par Microsoft et peut être transcompilé (retransformé) en JS normal. Je vous invite de nouveau sur sa [page wikipedia](https://fr.wikipedia.org/wiki/TypeScript) ansi que sur son [site officiel](https://www.typescriptlang.org/index.html).

Ces fichiers ont l'extension `.ts`. Pour les exemples ci-dessous j'ai utilisé mon IDE pour compiler directement mon fichier ts en fichier js.

Pour résumer les avantages de Typescipt.

1. Un typage strict: 
    Nous pouvons désormais déclarer un type à nos propriétés. Dans [cet exemple](typed_properties) nous déclarons que firstName et lastName doivent etre des `string`. Mais le type peut etre aussi n'importe quelle Classe. 
   
2. Des interfaces: 
    Les interface sont comme des sur-classes. C'est à dire qu'elle définissent les méthodes qui doivent être présentent dans les classes qui l'implémentent.
    
    Dans cet exemple, le fait d'avoir mis public devant les arguments du constructeur fait de l'argument une proprété de la classe.
    
    ```
    class Student implements Person{
        fullName: string;
        constructor(public firstName: string, public middleInitial: string, public lastName: string) {
            this.fullName = firstName + " " + middleInitial + " " + lastName;
        }
    }
    
    interface Person {
        firstName: string;
        lastName: string;
    }
    
    function greeter(person: Person) {
        return "Hello, " + person.firstName + " " + person.lastName;
    }
    
    let user = new Student("Jane", "M.", "User");
    
    document.body.textContent = greeter(user);
    ``` 

## Exercice pratique

Regardez cet [exercice](tp). Recréez le code source en vous inspirant de l'exemple, puis cherchez des manières de l'améliorer. Certains concepts n'ont pas encore été vus, mais parfois un petit exemple vaut mieux qu'un grand discours. 

[Retour](..)
