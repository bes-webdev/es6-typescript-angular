var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = /** @class */ (function () {
    function Person() {
    }
    Person.prototype.talk = function (message) {
        var messageWithName = this.firstName + ' ' + this.lastName + ' : ' + message;
        console.log(messageWithName);
    };
    return Person;
}());
var Wizzard = /** @class */ (function (_super) {
    __extends(Wizzard, _super);
    function Wizzard() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Wizzard.prototype.hit = function (victim) {
        victim.talk('ouch');
    };
    return Wizzard;
}(Person));
var joueur1 = new Person();
var joueur2 = new Person();
joueur1.firstName = "Marty";
joueur1.lastName = "Mac Fly";
joueur2.firstName = "Emmet";
joueur2.lastName = "Brown";
joueur1.talk("Salut Doc");
joueur2.talk("Nom de Zeus !");
//Si on veut assigner une valeur d'un mauvais type, nous aurons une erreur dans notre IDE.
// joueur2.talk(1);
