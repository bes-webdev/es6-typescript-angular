export class Person {

    firstName;
    lastName;

    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    talk(message){
        const messageWithName = this.firstName + ' ' + this.lastName + ' : ' + message;
        console.log(messageWithName);
    }
}
