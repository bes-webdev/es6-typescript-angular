# NPM

NPM est un gestionnaire de dépendances. Je vous propose de lire sa [fiche wikipedia](https://fr.wikipedia.org/wiki/Npm). 

En gros nous allons nous en servir pour créer nos projet, gérer ses dépendances (y intégrer les librairies, les maintenair à jour,...) et deux trois autres fonctionalités que nous allons voir au fur et à mesure.

## Installation
NPM s'installe avec NodeJS. Pour installer NodeJS je vous recommande d'utiliser NVM (Node Version Manager). Vous pouvez trouver ici la version [windows](https://github.com/coreybutler/nvm-windows) et ici la version [Linux/Mac](https://github.com/nvm-sh/nvm). 

Une fois NVM installé, vous pourrez installer node (et donc NPM) en tapant la ligne `nvm install node` dans le terminal de votre ordinateur. Ha oui, je ne vous l'avais pas encore dit mais à partir d'ici nous risquos de passer beaucoup de temps dans le terminal.

Maintenant que node est installé vous devriez avoir un résultat du type `6.13.0` quand vous tapez la commande `npm -v`. Il est possible que vous deviez redémarrer votre terminal pour que la commande fonctionne.


## Utilisation
Vous aurez principalement 4 commandes npm à faire lors de vos projets JS. NPM va créer un fichier `package.json` dans lequel il mettra la liste des dépendances de votre projet et leur verison. 

-  `npm install` Va lire le package.json et installer toutes les dépedandances qui y sont mentionnées. Les fichiers seront installés par défaut dans le dossier `node_modules` 

-  `npm install [packageName]` Va ajouter un package dans le fichier `package.json` et l'installer dans le dossier `node_modules`. avec l'option `-g` il va l'installer globalement.

-  `npm update` Va mettre à jour les dépendances de votre projet.

-  `npm start` Va lire ce qu'il est écrit dans le noeud `start` du package.json et va l'executer (pour nous il va lancer un server angular).


Pour plus de détails je vous invite à lire la documentation [ici](https://docs.npmjs.com/cli-documentation/).


[Retour](..)
